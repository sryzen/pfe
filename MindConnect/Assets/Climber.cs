﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;

public class Climber : MonoBehaviour
{
    private CharacterController character;
    public static XRController climbingHand;
    private ContinuousMovement continuousMouvement;
    private float wallJumpMaxTime;
    private float wallJumpTime;
    private float wallJumpMaxSpeed;
    private float wallJumpSpeed;
    private float wallJumpMaxHeight;
    private float wallJumpHeight;
    private bool wallJumping;
    private Vector3 wallJumpAngle;
    Quaternion headYaw;

    XRController climbingHandPreviousState;

    // Start is called before the first frame update
    void Start()
    {
        character = GetComponent<CharacterController>();
        continuousMouvement = GetComponent<ContinuousMovement>();
        wallJumpMaxTime = 2f;
        wallJumpAngle = Vector3.zero;
        wallJumpMaxSpeed = 5;
        wallJumpSpeed = wallJumpMaxSpeed;
        wallJumpMaxHeight = 5;
        wallJumpHeight = wallJumpMaxHeight;
        wallJumping = false;
        climbingHandPreviousState = null;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(climbingHand)
        {
            wallJumping = false;
            continuousMouvement.enabled = false;
            Climb();
        }
        else
        {
            continuousMouvement.enabled = true;
            if (climbingHandPreviousState)
                continuousMouvement.fallingSpeed = 0;
        }
        if (wallJumping)
            wallJumpLaunch();
        if (continuousMouvement.isGrounded)
            wallJumping = false;

        climbingHandPreviousState = climbingHand;
    }

    void Climb()
    {
        InputDevices.GetDeviceAtXRNode(climbingHand.controllerNode).TryGetFeatureValue(CommonUsages.deviceVelocity, out Vector3 velocity);
        InputDevices.GetDeviceAtXRNode(continuousMouvement.leftController).TryGetFeatureValue(CommonUsages.primaryButton, out bool jumpLeft);
        InputDevices.GetDeviceAtXRNode(continuousMouvement.rightController).TryGetFeatureValue(CommonUsages.primaryButton, out bool jumpRight);

        character.Move(transform.rotation * -velocity * Time.fixedDeltaTime);

        if (jumpLeft || jumpRight)
            wallJump();

    }

    void wallJump()
    {
        climbingHand = null;
        continuousMouvement.enabled = true;
        continuousMouvement.fallingSpeed = 0;
        //InputDevices.GetDeviceAtXRNode(continuousMouvement.rightController).TryGetFeatureValue(CommonUsages.deviceRotation, out Quaternion headYaw);
        //headYaw = Quaternion.Euler(0, headYaw.eulerAngles.y, 0);

        headYaw = Quaternion.Euler(0, continuousMouvement.rig.cameraGameObject.transform.eulerAngles.y - 45, 0);
        Vector3 direction = headYaw * new Vector3(wallJumpSpeed, wallJumpHeight, wallJumpSpeed);

        wallJumpSpeed = wallJumpMaxSpeed;
        wallJumpHeight = wallJumpMaxHeight;
        continuousMouvement.isGrounded = false;
        wallJumping = true;
        wallJumpTime = wallJumpMaxTime;
        wallJumpAngle = direction;
    }

    void wallJumpLaunch()
    {
        wallJumpTime -= 0.01f;

        if (wallJumpTime <= 0)
        {
            if (wallJumpSpeed > 0)
                wallJumpSpeed -= 0.1f;
            if (wallJumpHeight > 0)
                wallJumpHeight -= 0.1f;
            Vector3 direction = headYaw * new Vector3(wallJumpSpeed, 0, wallJumpSpeed) + new Vector3(0, wallJumpHeight, 0);
        }

        character.Move(wallJumpAngle * Time.fixedDeltaTime);
    }
}
