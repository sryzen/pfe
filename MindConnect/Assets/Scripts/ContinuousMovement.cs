﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;

public class ContinuousMovement : MonoBehaviour
{
    public float speed;
    public XRNode leftController;
    public XRNode rightController;
    public XRNode oculus;
    public float gravity = -9.81f;
    public LayerMask groundLayer;
    public float additionalHeigt = 0.2f;
    public int jumpForce = 10;

    public XRRig rig;
    public float fallingSpeed;
    private Vector2 inputAxis;
    private CharacterController character;

    public Vector3 leftControllerAccel;
    public Vector3 rightControllerAccel;
    public Vector3 oculusPos;

    public bool isGrounded;

    // Start is called before the first frame update
    void Start()
    {
        character = GetComponent<CharacterController>();
        rig = GetComponent<XRRig>();
    }

    // Update is called once per frame
    void Update()
    {
        InputDevice device = InputDevices.GetDeviceAtXRNode(leftController);
        device.TryGetFeatureValue(CommonUsages.primary2DAxis, out inputAxis);
        device.TryGetFeatureValue(CommonUsages.deviceAcceleration, out leftControllerAccel);

        device = InputDevices.GetDeviceAtXRNode(rightController);
        device.TryGetFeatureValue(CommonUsages.deviceAcceleration, out rightControllerAccel);

        device = InputDevices.GetDeviceAtXRNode(oculus);
        device.TryGetFeatureValue(CommonUsages.devicePosition, out oculusPos);
    }

    private void FixedUpdate()
    {
        CapsuleFollowHeadset();

        Quaternion headYaw = Quaternion.Euler(0, rig.cameraGameObject.transform.eulerAngles.y, 0);
        Vector3 direction = headYaw * new Vector3(inputAxis.x, 0, inputAxis.y);


        character.Move(direction * Time.fixedDeltaTime * speed);

        //gravity & jump
        isGrounded = CheckIfGrounded();

        if (leftControllerAccel.y > 20 && rightControllerAccel.y > 20 && isGrounded && fallingSpeed <= 0)
        {
            fallingSpeed += jumpForce;
            isGrounded = false;
        }

        if (isGrounded)
            fallingSpeed = 0;
        else
            fallingSpeed += gravity * Time.fixedDeltaTime;

        character.Move(Vector3.up * fallingSpeed * Time.fixedDeltaTime);
    }

    void CapsuleFollowHeadset()
    {
        character.height = rig.cameraInRigSpaceHeight + additionalHeigt;
        Vector3 capsuleCenter = transform.InverseTransformPoint(rig.cameraGameObject.transform.position);
        character.center = new Vector3(capsuleCenter.x, character.height / 2, capsuleCenter.z);
    }   
    bool CheckIfGrounded()
    {
        Vector3 rayStart = transform.TransformPoint(character.center);
        float rayLength = character.center.y + 0.1f;
        bool hasHit = Physics.SphereCast(rayStart, character.radius, Vector3.down, out RaycastHit hitInfo, rayLength, groundLayer);
        return hasHit;  
    }
}
